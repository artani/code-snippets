from django import forms
from .models import addNewStudent

class RateForm(forms.ModelForm):
  class Meta:
    model = addNewStudent
    fields = ['student_pin','rate']