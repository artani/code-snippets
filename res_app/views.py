from django.shortcuts import render
from django.views import generic
from django.template import RequestContext
from django.http import HttpRequest, HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy
from .forms import RateForm

# Create your views here
from .models import addNewStudent
def homepage(request):			
	allData  = addNewStudent.objects.all()
	showData = {"info": allData}
	return render(request, "results.wekonline", showData, context_instance = RequestContext(request))

def rate(request, primary_key):	
	std      =  addNewStudent.objects.get(id = primary_key)
	stdl     = RateForm(request.POST or None)
	inst     = stdl.save(commit = False)
	getAll   = std.rate
	getAll   = getAll + inst.rate
	std.rate = getAll
	std.save()
	

	# if stdl.is_valid():
	# 	inst.firstname = std.firstname
	# 	inst.lastname = std.lastname
	# 	inst.Subject  = std.Subject
	# 	inst.grade   = std.grade
	# 	inst.grade_level = std.grade_level
	# 	inst.rate     = inst.rate
	# 	if inst.student_pin != std.student_pin:
	# 		print "sorry pin is not right"
	# 	else:
	# 		inst.student_pin = std.student_pin
	# 		inst.save()
	return render(request, "update.wekonline", {"pk": getAll, "data": stdl, "student": std})