from django.contrib import admin

# Register your models here.
from .models import addNewStudent, AddSubject
class addNewStudentShowFields(admin.ModelAdmin):
	list_display = ("__unicode__", "firstname", "lastname", "rate", "grade", "grade_level")

	class Meta:
		model = addNewStudent

admin.site.register(addNewStudent, addNewStudentShowFields)
admin.site.register(AddSubject)