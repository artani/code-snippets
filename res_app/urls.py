from django.conf.urls import url
from . import views

urlpatterns = [

    url(r'^rate/(?P<pk>[0-9])$', views.rate, name='rate_student'),

]