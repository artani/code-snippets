# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('res_app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AddSubject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subjectTitle', models.CharField(max_length=40)),
            ],
        ),
        migrations.AddField(
            model_name='addnewstudent',
            name='student_pin',
            field=models.IntegerField(default=False),
        ),
        migrations.AddField(
            model_name='addnewstudent',
            name='addsubject',
            field=models.ManyToManyField(to='res_app.AddSubject'),
        ),
    ]
