# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('res_app', '0004_auto_20150927_1837'),
    ]

    operations = [
        migrations.AlterField(
            model_name='addnewstudent',
            name='rate',
            field=models.IntegerField(blank=True),
        ),
    ]
