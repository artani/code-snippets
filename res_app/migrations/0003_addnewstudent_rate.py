# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('res_app', '0002_auto_20150927_1810'),
    ]

    operations = [
        migrations.AddField(
            model_name='addnewstudent',
            name='rate',
            field=models.IntegerField(default=0),
        ),
    ]
