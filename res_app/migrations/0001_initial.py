# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='addNewStudent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('firstname', models.CharField(max_length=25)),
                ('lastname', models.CharField(max_length=26)),
                ('description', models.CharField(max_length=300)),
                ('grade', models.CharField(max_length=15)),
                ('grade_level', models.CharField(max_length=20)),
            ],
        ),
    ]
