# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('res_app', '0003_addnewstudent_rate'),
    ]

    operations = [
        migrations.RenameField(
            model_name='addnewstudent',
            old_name='description',
            new_name='Subject',
        ),
    ]
