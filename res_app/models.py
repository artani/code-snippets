from django.db import models
from django.core.urlresolvers import reverse
from django.forms.models import model_to_dict
# Create your models here.


#______________________________
# AddSubject not used anymore |
#------------------------------
class AddSubject(models.Model):
	subjectTitle = models.CharField(blank = False, max_length = 40)

	def __unicode__(self):
		return self.subjectTitle


#-----------------------------------|
# addStudentModel for adding fields |
#-----------------------------------|

class addNewStudent(models.Model):
	firstname   = models.CharField(blank = False, max_length = 25)
	lastname    = models.CharField(blank = False, max_length = 26)
	Subject     = models.CharField(max_length = 300)
	addsubject  = models.ManyToManyField(AddSubject)
	grade       = models.CharField(max_length = 15)
	grade_level = models.CharField(max_length = 20)
	rate        = models.IntegerField(default = 0)	
	student_pin = models.IntegerField(default = False)	

	def __unicode__(self):
		dict = model_to_dict(self)
		return "{firstname}".format(**dict)

	def get_absolute_url(self):
		return reverse('rate', kwargs={'pk': self.pk})