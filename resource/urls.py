"""resource URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from res_app import views as res_views
from res_app import urls as res_url

urlpatterns = [  
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', res_views.homepage, name='homepage'),
    url(r'^rate/(?P<primary_key>[0-9])$', res_views.rate, name='rate_student'),

]
